package duel

object Solver {

  def createMatrix(input: List[List[String]]): List[List[String]] = {
    val size = input.size
    val rightSize = Math.pow(2, Math.ceil(Math.log(size) / Math.log(2))).toInt

    def emptyRow = List.fill(rightSize)("")

    val fixedColumns = for {
      row <- input
    } yield row ++ List.fill(rightSize - size)("")

    fixedColumns ++ List.fill(rightSize - size)(emptyRow)
  }

  def shrink(input: List[List[Int]], size: Int) = {
    (for {
      i <- 0 until size
    } yield input(i).take(size)).toList
  }

  def solve(input: List[List[String]]): List[List[Int]] = {
    val size = input.size

    val elements = createMatrix(input).map(l => l.map(Element.createElement))

    val possibleResults = loop(elements)

    val result = possibleResults.find(res =>
      res.top.forall(_ == false) &&
      res.bottom.forall(_ == false) &&
      res.left.forall(_ == false) &&
      res.right.forall(_ == false)
    ).get

    val answer = shrink(result.toArray, size)

    answer
  }

  def loop(input: List[List[List[AbstractBlock]]]): List[AbstractBlock] = {
    if(input.size == 1) input(0)(0)
    else {
      loop(iterate(input))
    }
  }

  def iterate(input: List[List[List[AbstractBlock]]]): List[List[List[AbstractBlock]]] = {
    def indexes = (0 until input.size).filter(_%2==0)

    val newBlocks = (for {
      i <- indexes
      j <- indexes
    } yield combine(
      input(i)(j),
      input(i)(j+1),
      input(i+1)(j),
      input(i+1)(j+1)
    )).toList

    newBlocks.grouped(input.size / 2).toList
  }

  def combine(
               first: List[AbstractBlock],
               second: List[AbstractBlock],
               third: List[AbstractBlock],
               fourth: List[AbstractBlock]
               ): List[AbstractBlock] = {

    val rawBlocks = for {
      b1 <- first
      b2 <- second
      b3 <- third
      b4 <- fourth
    } yield new Block(b1, b2, b3, b4)

    rawBlocks.filter(_.valid)
  }
}

package duel


object Element {
  def createElement(s: String): List[AbstractBlock] = s match {
    case "d" => (0 to 3).map(i => new ElementD(i)).toList
    case "i" => (0 to 1).map(i => new ElementI(i)).toList
    case "l" => (0 to 3).map(i => new ElementL(i)).toList
    case "t" => (0 to 3).map(i => new ElementT(i)).toList
    case "x" => List(new ElementX(0))
    case "" => List(new ElementO)
  }
}

abstract class Element extends AbstractBlock {
  override def valid: Boolean = true

  override def toArray: List[List[Int]] = List(List(this.getRotation))

  def getRotation: Int
}

class ElementD(rotation: Int) extends Element{

  override def bottom: List[Boolean] = List(rotation == 0)

  override def top: List[Boolean] = List(rotation == 2)

  override def left: List[Boolean] = List(rotation == 3)

  override def right: List[Boolean] = List(rotation == 1)

  override def getRotation: Int = rotation
}

class ElementI(rotation: Int) extends Element{

  override def bottom: List[Boolean] = List(rotation == 0  || rotation == 2)

  override def top: List[Boolean] = List(rotation == 0  || rotation == 2)

  override def left: List[Boolean] = List(rotation == 1  || rotation == 3)

  override def right: List[Boolean] = List(rotation == 1  || rotation == 3)

  override def getRotation: Int = rotation
}

class ElementL(rotation: Int) extends Element{

  override def bottom: List[Boolean] = List(rotation == 2 || rotation == 3)

  override def top: List[Boolean] = List(rotation == 0 || rotation == 1)

  override def left: List[Boolean] = List(rotation == 1 || rotation == 2)

  override def right: List[Boolean] = List(rotation == 0 || rotation == 3)

  override def getRotation: Int = rotation
}

class ElementT(rotation: Int) extends Element{
  override def right: List[Boolean] = List(rotation != 3)

  override def bottom: List[Boolean] = List(rotation != 2)

  override def top: List[Boolean] = List(rotation != 0)

  override def left: List[Boolean] = List(rotation != 1)

  override def getRotation: Int = rotation
}

class ElementX(rotation: Int) extends Element{

  override def bottom: List[Boolean] = List(true)

  override def top: List[Boolean] = List(true)

  override def left: List[Boolean] = List(true)

  override def right: List[Boolean] = List(true)

  override def getRotation: Int = rotation
}

class ElementO extends Element {
  override def getRotation: Int = 0

  override def bottom: List[Boolean] = List(false)

  override def top: List[Boolean] = List(false)

  override def left: List[Boolean] = List(false)

  override def right: List[Boolean] = List(false)
}

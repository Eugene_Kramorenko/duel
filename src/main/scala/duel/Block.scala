package duel

class Block(
             first: AbstractBlock,
             second: AbstractBlock,
             third: AbstractBlock,
             fourth: AbstractBlock
             ) extends AbstractBlock{


  override def valid: Boolean = {
    first.right == second.left &&
    first.bottom == third.top &&
    third.right == fourth.left &&
    second.bottom == fourth.top
  }

  override def right: List[Boolean] = second.right ++ fourth.right

  override def bottom: List[Boolean] = third.bottom ++ fourth.bottom

  override def left: List[Boolean] = first.left ++ third.left

  override def top: List[Boolean] = first.top ++ second.top

  private def horizConcat(left: List[List[Int]], right: List[List[Int]]): List[List[Int]] = {
    val size = left.size
    (for {
      i<- 0 until size
    } yield left(i) ++ right(i)).toList
  }


  override def toArray: List[List[Int]] = {
    horizConcat(first.toArray, second.toArray) ++
    horizConcat(third.toArray, fourth.toArray)
  }

  override def toString(): String = toArray.toString
}

package duel

abstract class AbstractBlock {
  def valid: Boolean
  def right: List[Boolean]
  def top: List[Boolean]
  def left: List[Boolean]
  def bottom: List[Boolean]

  def toArray: List[List[Int]]
}

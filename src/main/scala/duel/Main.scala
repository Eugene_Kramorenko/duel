package duel

import com.google.gson.Gson
import scala.collection.convert.wrapAsScala._
import scalaj.http._


object Main {

  def main(args: Array[String]) {

    while(true) {

      val getCommand = Http("http://2015.anahoret.com/api/puzzle?token=31e10cfd-8da5-11e4-9467-040136af6601&dimension=41").asString
      val input = getCommand.body

      println(input)

      val inputHolder = new Gson().fromJson(input, classOf[InputHolder])

      val solution = Solver.solve(inputHolder.getShapes.map(_.toList).toList)
      val solutionString = "[" + solution.map("[" + _.mkString(", ") + "]").mkString(",") + "]"

      println(solutionString)

      val response = Http("http://2015.anahoret.com/api/puzzle").postForm(Seq(
        "token" -> "31e10cfd-8da5-11e4-9467-040136af6601",
        "id" -> inputHolder.getId.toString,
        "solution" -> solutionString
      )).asString

      println(response.body)
    }
  }
}

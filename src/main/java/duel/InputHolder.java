package duel;

import java.util.List;

public class InputHolder {

    private Integer id;
    private Integer dimensions;
    private List<List<String>> shapes;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getDimensions() {
        return dimensions;
    }

    public void setDimensions(Integer dimensions) {
        this.dimensions = dimensions;
    }

    public List<List<String>> getShapes() {
        return shapes;
    }

    public void setShapes(List<List<String>> shapes) {
        this.shapes = shapes;
    }
}
